import React, { Component } from 'react';
import {firebase} from '../Firebase';
import user_img from '../../img/user_img.png';

class NavState extends Component {
    state = { 
        MemberNum: 0
    }
    getUserStateColor = () => {
        return (this.state.user_state === "online") ? "green" : "orange";
    }
    getMemberNum = () => {
        this.chatroomRef = firebase.database().ref('Chatroom');
        this.chatroomRef.child(this.props.room_name).child('member_num').on('value', (snapshot) => {
            var Num = snapshot.val();
            this.setState({MemberNum: Num});
        })
    }
    componentDidMount() {
        this.getMemberNum();
    }
    componentWillUnmount() {
        this.chatroomRef.off();
    }

    render() { 
        return ( 
            <li onClick={ () => this.props.ChangeRoom(this.props.room_name) }>
                <img src={user_img} alt="user"></img>
                <div>
                    <h2>{this.props.room_name}</h2>
                    <h3>
                        {/* <span className={"status "+ this.getUserStateColor()} ></span> */}
                        {this.state.MemberNum} members
                    </h3>
                </div>
            </li>
        );
    }
}
 
export default NavState;