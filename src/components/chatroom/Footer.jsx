import React, { Component } from 'react';
import {firebase} from '../Firebase';

class Footer extends Component {
    state = { 
        text: "",
        user_email: this.props.user_email,

    }
    text_change = (e) => {
        this.setState({text: e.target.value});
    }

    SendMessages = () => {
        var email = this.props.user_email;
        var loc = "Chatroom/" + this.props.cur_room_id;
        console.log(loc);
        var text = this.state.text;

        var today = new Date();
        var time = (today.getMonth() + 1) + '/' + today.getDate() + ' ' + today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
        firebase.database().ref(loc+'/messages').push({
            "email" : email,
            "data" : text,
            "time" : time
        });
        firebase.database().ref(loc).child('msg_num').get().then(snapshot => {
            if(snapshot.exists()) {
                var num = snapshot.val();
                firebase.database().ref(loc).child('msg_num').set(num+1);
            }
            else{
                firebase.database().ref(loc).child('msg_num').set(1);
            }

        })
        this.setState({text:""}); 
    }    

    render() { 
        return (
            <footer>
            <textarea placeholder="Type your message here" value={this.state.text} onChange={this.text_change}></textarea>

            {/* <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1940306/ico_picture.png" alt="" />
            <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1940306/ico_file.png" alt="" /> */}
            <button onClick={this.SendMessages}>Send</button>
            </footer>
        );
    }
}
 
export default Footer;