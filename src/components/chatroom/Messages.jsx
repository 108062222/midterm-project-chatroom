import React, { Component } from 'react';


class Messages extends Component {
    state = {
        cur_user_email: this.props.cur_user_email,
    }
    getWho = () => {
        return (this.props.content.email===this.state.cur_user_email) ? "me" : "you";
    }
    getColor = () => {
        return (this.getWho()==='me') ? 'blue' : 'green';
    }
    render() { 
        return (
            <li key={this.props.content.id} className={this.getWho()}>
                <div className="entete">
                    <span className={"status "+ this.getColor()}></span>
                    <h2>{this.props.content.email}</h2>
                    <br/>
                    <h3>{this.props.content.time}</h3>
                </div>
                <div className="triangle"></div>
                <div className="message">
                    {this.props.content.data}
                </div>
                
            </li>            
        );
    }
}
 
export default Messages;