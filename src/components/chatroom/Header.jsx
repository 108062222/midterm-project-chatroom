import React, { Component } from 'react';
import user_img from '../../img/user_img.png';
import {firebase} from '../Firebase';

class Header extends Component {
    state = {
        
    }
    Log_out = () => {
        firebase.auth().signOut();
    }
    render() { 
        return (            
            <header>
                <img src={user_img} alt="" />
                <div>
                    <h2>Chatroom: 
                        <span style={{'--i':'1'}}>{this.props.cur_room_id}</span>                       
                    </h2>
                    <h3>already has {this.props.msg_num} messages</h3>
                </div>
                <button className="btn btn-danger btn-danger" id="log_out" onClick={this.Log_out}>Log Out</button>            
            </header>
        );
    }
}
 
export default Header;