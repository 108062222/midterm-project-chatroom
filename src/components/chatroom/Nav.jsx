import React, { Component } from 'react';
import NavState from './NavState';
import add_friend from '../../img/add_friend.png';

class ChatroomNav extends Component {
    state = { 
        RoomName: "",
        text: "",
    }
    textEnter = (e) => { 
        if(e.key === "Enter" && this.state.text !== ""){
            console.log("press enter");
            this.props.EnterRoom(this.state.text);
            this.setState({text:""});
        }        
    }
    imgEnter = () => {
        if(this.state.text !== ""){
            console.log("press add img");
            this.props.EnterRoom(this.state.text);
            this.setState({text:""});
        }        
    }
    textChange = (e) => {
        this.setState({text: e.target.value});
    }
    render() { 
        return ( 
            <aside>
                <header>
                    <input type="text" placeholder="Enter chatroom name" value={this.state.text} onChange={this.textChange} onKeyUp={this.textEnter}/>
                    <img src={add_friend} alt='search' onClick={this.imgEnter}></img>
                </header>
                <ul>                
                    {this.props.rooms.map( (room) => <NavState key={room.key} room_name={room.name} ChangeRoom={this.props.ChangeRoom} /> )}
                </ul>
                </aside>
        );
    }
}
 
export default ChatroomNav;