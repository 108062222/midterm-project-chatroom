import React, { Component } from 'react';
import {firebase} from './Firebase';

export default class LoginPage extends Component {
    constructor(props) {
        super(props);
        this.state = {  
            user_email: "",
            user_password: "",
            user: null
        }
    }
    Email_change = (e) => {
        this.setState({user_email: e.target.value});
    }
    Pwd_change = (e) => {
        this.setState({user_password: e.target.value});
    }
    LoginWithEmailandPassword = () => {
        firebase.auth().signInWithEmailAndPassword(this.state.user_email, this.state.user_password).then(() => {
            console.log("success login");},
            function(error) {            
            console.log(error.code, error.message);
        });        
    }
    LoginWithGoogle = async () => {
        var tmp = null;
        const provider = new firebase.auth.GoogleAuthProvider();
        await firebase.auth().signInWithPopup(provider).then( (result) => {
            tmp = result.user;
        }, (error) => {
            console.log(error.code, error.message);
        });

        if(tmp) {
            this.setState({user: tmp});
        }
    }
    LoginWithFacebook = async () => {
        var tmp = null;
        var provider = new firebase.auth.FacebookAuthProvider();
        await firebase.auth().signInWithPopup(provider).then(function(result) {
            // var token = result.credential.accessToken;            
            tmp = result.user;
        }).catch(function(error) {
            // var errorCode = error.code;
            // var errorMessage = error.message;
            // var credential = error.credential; 
            console.log(error); 
        });

        if(tmp) this.setState({user: tmp});          
    }
    SignUp = () => {
        firebase.auth().createUserWithEmailAndPassword(this.state.user_email, this.state.user_password).then(() => {
            alert("SignUp Success!");
        }, () => {
            alert("SignUp Failure!");
        });
    }
    
    render() { 
        return ( 

            <div className="container">
                <div className="row border border-dark rounded mt-4 pd-2">
                    <div className="container-sm pd-2">
                        <h1 className="h3 font-weight-normal row mx-2">Welcome to Chatroom</h1>
                        <p className="row mx-2">Please log in or sign up</p>

                        <label htmlFor="email_input" className="form-label row mx-2">Email address:</label>                
                        <input type="email" className="form-control row mx-2" id="email_input" placeholder="Enter your email" required autoFocus
                        value = {this.state.user_email} onChange={this.Email_change}/>
                        <label htmlFor="pw_input" className="form-label row mx-2 mt-1">Password:</label>
                        <input type="password" className="form-control  mx-2 row " id="pw_input" placeholder="Enter your password" required
                        value = {this.state.user_password} onChange={this.Pwd_change}/>

                        <div className="row">
                        <button className="btn btn-lg btn-primary mt-2 mx-auto col-md-4 col-sm-11" id="btnLogin"
                        onClick={this.LoginWithEmailandPassword}
                        >Log in</button>
                        <button className="btn btn-lg btn-info mt-2 mx-auto col-md-7 col-sm-11" id="btngoogle"
                        onClick={this.LoginWithGoogle}
                        >Log in with Google</button>
                        </div>
                        <div className="row mb-2">
                        <button className="btn btn-lg btn-success mt-2 mx-auto col-md-4 col-sm-11" id="btnSignUp"
                        onClick={this.SignUp}
                        >Sign up</button>
                        <button className="btn btn-lg btn-secondary mt-2 mx-auto col-md-7 col-sm-11" id="btnFB"
                        onClick={this.LoginWithFacebook}
                        >Log in with FaceBook</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
