import firebase from 'firebase/app'
import 'firebase/auth';
import 'firebase/database';

const config = {
    apiKey: "AIzaSyD7VrqnHfakhuLuKLc3MbwkSs4TLVQbosY",
    authDomain: "software-studio-25188.firebaseapp.com",
    databaseURL: "https://software-studio-25188-default-rtdb.firebaseio.com",
    projectId: "software-studio-25188",
    storageBucket: "software-studio-25188.appspot.com",
    messagingSenderId: "232875028621",
    appId: "1:232875028621:web:24af15388e4aa80feeb2c4",
    measurementId: "G-X2P7PPN3XR"
}

function initFirebase() {
    if(!firebase.apps.length) firebase.initializeApp(config);
}
initFirebase();

export {firebase};
