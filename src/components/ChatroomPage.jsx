import React, { Component } from 'react';
import {firebase} from './Firebase';
import '../index.css';

import ChatroomNav from './chatroom/Nav';
import Header from './chatroom/Header';
import Messages from './chatroom/Messages';
import Footer from './chatroom/Footer';

class ChatroomPage extends Component {
    state = { 
        user: this.props.user,
        messages: [],
        rooms: [

        ],
        cur_room_id: 'public',
        messages_num: 0
    }
        
    getMsgNum = (name) => {
        if(this.MsgNumRef){
            this.MsgNumRef.off();
        }        
        var room_name = (name) ? name : this.state.cur_room_id;
        this.MsgNumRef = firebase.database().ref('Chatroom').child(room_name).child('msg_num');
        this.MsgNumRef.on('value', (snapshot) =>{
            if(snapshot.exists()){
                console.log(snapshot.val());                
                this.setState({messages_num: snapshot.val()})
            }
        })
        console.log("getMsgNum");
    }

    //triger by search namme
    EnterRoom = (room_name) => {
        this.User_on_Server.get().then((snapshot) => {
            var user_data = snapshot.val();
            if(!user_data['inChatroom'].includes(room_name)) {
                user_data['inChatroom'].push(room_name);
                this.User_on_Server.set(user_data);
                var cur_room_ref = firebase.database().ref('Chatroom').child(room_name);
                cur_room_ref.get().then(snapshot => {
                    var room = snapshot.val();
                    if(room){
                        console.log("existed room");          
                        cur_room_ref.child('member_num').set(room.member_num + 1);
                    }
                    else{
                        console.log("new room");
                        cur_room_ref.child('member_num').set(1);
                        cur_room_ref.child('msg_num').set(0);
                    }
                }).catch((e)=>{
                    console.log(e.message);
                });
            }
        });
        this.getHistory(room_name);
    }
    getHistory = async (room_id) => {
        await this.getMsgNum(room_id);

        if(this.ChatroomRef) this.ChatroomRef.off();
        if(room_id){
            this.ChatroomRef = firebase.database().ref('Chatroom/'+ room_id +'/messages');        
            this.setState({
                cur_room_id: room_id,
                messages: [],
            })
            console.log("in getHistory1");
            // console.log(room_id);
            // console.log(this.state.cur_room_id);
        }
        else{
            this.ChatroomRef = firebase.database().ref('Chatroom/'+ this.state.cur_room_id +'/messages');
        }        
        var Ref = this;
        var alldata = [];
        var key = 0;
        var preRoom = 'public';
        var room_name = (room_id) ? room_id : this.state.cur_room_id;
            
        this.ChatroomRef.on('child_added', (data) => {
            var ChildData = data.val();
            ChildData['key'] = key++;
            alldata.push(ChildData);
            Ref.setState({messages: alldata});
            console.log(this.state.messages_num);
            console.log('key: ', key);

            if(preRoom === room_name){
                if(key > this.state.messages_num && ChildData.email !== this.state.user.email){
                    console.log('notify');
                    new Notification('New Message from Room '+ room_name);
                }
            }
            else{
                preRoom = room_name;
            }
        });
    }
    getValidRoom = () => {
        var user_email = this.state.user.email.replaceAll('.',',');
        this.User_on_Server = firebase.database().ref('users/'+ user_email);
        var Ref = this;

        this.User_on_Server.child('inChatroom').on('value', (data) => {
            var allrooms = [];
            var room_key = 0;
            var name_ary = data.val();
            console.log(name_ary);
            for(var n in name_ary){
                var room_with_key = {
                    name: name_ary[n],
                    key: room_key++
                }
                allrooms.push(room_with_key);
            }
            console.log("all is ", allrooms);
            Ref.setState({rooms: allrooms});
        })
    }
    componentDidMount() {
        this.getValidRoom();
        this.getHistory();        
        this.getMsgNum();

        if (!("Notification" in window)) {
            console.log("This browser does not support desktop notification");
        } else {
            Notification.requestPermission();
        }
    }
    componentDidUpdate(prevProps, prevState) {
       
    }
    
    componentWillUnmount() {
        this.ChatroomRef.off();
        this.User_on_Server.off();
        this.MsgNumRef.off();
    }
    render() { 
            return (   
                <section>
                <div id="container">
                    <ChatroomNav rooms={this.state.rooms} EnterRoom={this.EnterRoom} ChangeRoom={this.getHistory}/>
                    <main>
                        <Header cur_room_id={this.state.cur_room_id} msg_num={this.state.messages_num}/>
                        <ul id="chat">                            
                            {this.state.messages.map(message => <Messages key={message.key} content={message} cur_user_email={this.state.user.email} />)}
                            {/* {[<Messages key={123} content={{email:1,data:"23",time:1}} cur_user_email={this.state.user.email} />,
                            <Messages key={122} content={{email:1,data:"789",time:1}} cur_user_email={this.state.user.email} />]} */}
                        </ul>
                        <Footer user_email={this.state.user.email} uid={this.state.user.uid} cur_room_id={this.state.cur_room_id}/>
                    </main>
                </div>
                </section>
            )
    };
}

export default ChatroomPage;