import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import LoginPage from './components/LoginPage';
import ChatroomPage from './components/ChatroomPage';

import {firebase} from './components/Firebase';

class Page extends Component {
    state = {
        user: null
    }    

    componentDidMount() {
        firebase.auth().onAuthStateChanged(user => {          
          if (user) {
            console.log("user logged in!");
            var user_email = user.email.replaceAll('.',',');
            console.log('user email: ', user_email);
            const dbRef = firebase.database().ref();
            dbRef.child("users").child(user_email).get().then((snapshot) => {
              if (snapshot.exists()) {
                console.log("user existed");
              } else {
                console.log("new user");
                dbRef.child("users").child(user_email).set({
                  inChatroom: ['public']
                })
                dbRef.child('Chatroom').child('public').get().then((snapshot) => {
                  var room = snapshot.val();
                  console.log("room is ", room);
                  dbRef.child('Chatroom').child('public').child('member_num').set(room.member_num + 1);
                })
              }
            }).catch((error) => {
              console.error(error);
            });
            this.setState({
              user: user
            });
          } else {
            console.log("user not logged in!");
            this.setState({
                user: null
            });
          }
        });
    }
    
    
    render() {
        return (
            <div>            
                {(this.state.user) ? <ChatroomPage user={this.state.user} /> : <LoginPage />}
            </div>           

        );
    }
}

ReactDOM.render(<Page />, document.getElementById("root"));

