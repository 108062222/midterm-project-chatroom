var webpack = require('webpack');
//ppt p84
module.exports = {
	entry: ["./src/index.js"],
    output: {
        filename: "compiled.js",
		publicPath: '/compiled_distination'
    },
	resolve: {
        extension: ['.js']
    },
	module: {
		rules: [{
			test: /\.(js)$/,
	        loader: 'babel-loader',
	        exclude: /node_modules/,
	        options: {
	            presets: ['@babel/preset-react', '@babel/preset-env']
	        },
		 }]
	},
	plugins: 
		[
			new webpack.HotModuleReplacementPlugin(), 
		    new webpack.ProvidePlugin({ 
		    	React: 'react',
		        ReactDOM: 'react-dom'
		    })
		],
		
	mode: 'production',
	devServer: {
        hot: true,
        compress: true,
        host: 'localhost',
        port: 8080
	}
};
