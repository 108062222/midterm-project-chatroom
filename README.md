# Software Studio 2021 Spring Midterm Project

## Topic
* Project Name : 108062222_online_chatroom

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

# 作品網址：https://software-studio-25188.web.app/


# Components Description : 
1. Membership Mechanism : Sign In 頁面的四個button就是，Create Account的話
是在上面兩個input輸入你要申請的帳號，再點擊Create Account。
2. Chatroom : Chatroom頁面左上角的input field可以輸入要加入的room名稱，輸入後按Enter或點擊旁邊的
圖示執行搜尋，如果沒有該room的話就會自動create新的room。
然後訊息的部分就在右下角，輸入後按send就送出。
3. Chrome notification : 當你停留在的chatroom有新的訊息時，就會寄送通知。


# Other Functions Description : 
1. Chatroom name 底下會顯示目前在這個Chatroom總共有多少訊息
2. Chatroom name 旁邊有一個 Log out buuton 可以讓user登出換成別的帳戶
3. 每則寄出的 message 會顯示是誰寄的，還有寄出的時間。
